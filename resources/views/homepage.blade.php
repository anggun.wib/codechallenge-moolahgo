<!DOCTYPE html>
<html lang="en">
<head>
  <title>AIS Demo</title>

 <!--  <link rel="stylesheet" href="{{ url('/assets/css/main.css') }}"> -->

  <!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> -->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry"></script>
  <script src="{{ url('assets/js/jquery-1.9.1.min.js')}}" type="text/javascript"></script>
  <script src="{{ url('assets/js/googlemap.js')}}" type="text/javascript"></script>
  <script src="{{ url('assets/js/jquery.tablednd.js')}}" type="text/javascript"></script>



  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<body>

<div class="container">
	<h3>Simulasi Route</h3>
		 <!-- <div id="map_canvas" style="width:100%; height: 300px; border-style: groove;">map area</div>  -->
		 <div id="dvMap" style="width:100%; height: 300px; border-style: groove;"> </div>
		<br/>
		<form class="form-inline" action="#">
		    <label for="lat">Latitude</label> 
		    <input type="text" class="form-control" id="lat" placeholder="Location Latitude">  
		    <label for="long">Longitude</label> 
		    <input type="text" class="form-control" id="long" placeholder="Location Longitude"> 
		    <label for="long">Rata-rata Kecepatan</label> 
		    <input type="text" class="form-control" id="txtAvgSpeed" placeholder="Rata-rata Kecepatan">
		</form><br/>
	                                                                                    
  <div class="table-responsive">          
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Nama Kantor</th>
        <th>Area</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Jarak (Meter)</th> 
        <th>Waktu (Menit)</th>         
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1;
            if (count($data) > 0) {
            ?>
            @foreach($data as $key => $value)
		      <tr>      	
		        <td>{{$value->id}}</td>
		        <td>{{$value->office_name}}</td>
		        <td>{{$value->areas}}</td>
		        <td>{{$value->latitude}}</td>
		        <td>{{$value->longitude}}</td> 
		        <td></td>
		        <td></td>
		        <td></td>
		      </tr>      
        	@endforeach       
       <?php } else { ?>
            <tr><td colspan="5" style="text-align: center;">Data Kosong</td></tr><?php } ?>
    </tbody>
  </table>
  </div>
</div>
</body>

<script type="text/javascript" language="javascript">
     InitializeMap();

</script>

</html>