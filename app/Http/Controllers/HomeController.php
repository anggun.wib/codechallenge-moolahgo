<?php

namespace App\Http\Controllers;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function showHomepage()
    {
        //return view('user.profile', ['user' => User::findOrFail($id)]);
        $offices = DB::select("SELECT * FROM offices"); 
        return view('homepage',['data' => $offices ]);
    }

    //
}
